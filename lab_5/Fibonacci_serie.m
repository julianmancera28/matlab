
function [] = Fibonacci()
for n = 0:10
    fprintf('Fib(%d)= %d\n', n, Fibonacci(n));    
    
end
end


function [] = Fibonacci_serie(n)
for n = 0:10
    fprintf('Fibonacci(%d)= %d\n', n, Fibonacci(n));    
    
end
end


function  result = Fibonacci(a)
if a==0||a==1
    result = a;       
else
    result = Fibonacci(a-2)+Fibonacci(a-1);    
end
end
